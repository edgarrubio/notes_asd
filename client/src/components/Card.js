import React, { Fragment } from "react";
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/core/'
import SvgIcon from '@material-ui/core/SvgIcon';
import TextField from '@material-ui/core/TextField';


// Show a check icon when messages is changed && pass the message if message != data.message

const styles = {
  card: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
};

function SimpleCard(props) {
  const { classes } = props;
  const message = props.data.message;
  return (
    <Fragment>
      <Card className={classes.card}>
        <CardHeader
          title=""
          subheader={props.data.updatedAt} />
        <CardContent>
          <Typography className={classes.title} color="textSecondary" gutterBottom>
            Note #{props.data.id}
          </Typography>
          {/* 
          <Typography variant="h5" component="h2">
            {props.data.message}
          </Typography>
          */}
          <Typography className={classes.pos} color="textSecondary">
            {props.data.message}
          </Typography>
          {/* 
          <TextField
            id="standard-uncontrolled"
            defaultValue={props.data.message}
            margin="normal"
            onChange={props.function2}
          />
          */}
        </CardContent>
        <CardActions>
          <IconButton aria-label="Delete" onClick={props.function}>
            <SvgIcon>
              <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                <path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z" />
              </svg>
            </SvgIcon>
          </IconButton>
          {/* 
          <IconButton>
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
              <path d="M14 10H2v2h12v-2zm0-4H2v2h12V6zM2 16h8v-2H2v2zm19.5-4.5L23 13l-6.99 7-4.51-4.5L13 14l3.01 3 5.49-5.5z" />
            </svg>
          </IconButton>
          */}
        </CardActions>
      </Card>
      {isBr(props.br)}
    </Fragment>
  );
}

function isBr(isBr) {
  return isBr ? <br /> : ""
}

SimpleCard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SimpleCard);